# _xframe_allow_webvisor_

## Чем занимается?

> Добавляет метатег в head для разрешения работы WebVisor

## Требования к платформе

- _Drupal 8-10_
- _PHP 7.4.0+_

## Версии

- [Drupal.org prod версия](https://www.drupal.org/project/xframe_allow_webvisor)

```sh
composer require 'drupal/xframe_allow_webvisor'
```

- [Drupal.org dev версия](https://www.drupal.org/project/xframe_allow_webvisor/releases/8.x-1.x-dev)

```sh
composer require 'drupal/xframe_allow_webvisor:1.x-dev@dev'
```

## Как использовать?

- Дополнительных настроек не требуется
