<?php

namespace Drupal\xframe_allow_webvisor\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribing an event.
 */
class XframeSubscriber implements EventSubscriberInterface {

  /**
   * Executes actions on the respose event.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   Filter Response Event object.
   */
  public function onKernelResponse(ResponseEvent $event) {
    $response = $event->getResponse();
    $response->headers->set('content-security-policy', "frame-ancestors 'self' http://webvisor.com https://webvisor.com https://metrika.yandex.ru http://metrika.yandex.ru");
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onKernelResponse'];
    return $events;
  }

}
